import { CarritoService } from './../../service/carrito.service';
import { Component, OnInit } from '@angular/core';
import { URL } from '../../config/url.service';
import { ProductosService } from '../../service/productos.service';
import { resolve } from 'url';


@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {
  public productos:any[]=[];
  public Url:String;

  constructor(private productosService:ProductosService, private carrito:CarritoService) {
      this.Url=URL;
   }

  ngOnInit() {
    this.mostrar_productos();
    this.Url=URL;
  }

  mostrar_productos(){
    this.productosService.cargar_todos().subscribe((response:any)=>{
      this.productos=response.products;
        console.log(this.productos);
    })
  }

  agregarCarrito(producto:any){

    this.carrito.agregar_carrito(producto);
  }




}
