import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {  HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CarritoComponent } from './page/carrito/carrito.component';
import { CategoriasComponent } from './page/categorias/categorias.component';
import { LoginComponent } from './page/login/login.component';
import { LordenesComponent } from './page/lordenes/lordenes.component';
import { OrdenesDetalleComponent } from './page/ordenes-detalle/ordenes-detalle.component';
import { PorCategoriaComponent } from './page/por-categoria/por-categoria.component';
import { ProductoComponent } from './page/producto/producto.component';
import { TabsComponent } from './page/tabs/tabs.component';
import { NavbarComponent } from './shared/navbar/navbar.component';

@NgModule({
  declarations: [
    AppComponent,
    CarritoComponent,
    CategoriasComponent,
    LoginComponent,
    LordenesComponent,
    OrdenesDetalleComponent,
    PorCategoriaComponent,
    ProductoComponent,
    TabsComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
