import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { URL } from '../config/url.service';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {

  public pagina:Number=0;
  public url:String;

  constructor(private http:HttpClient) {
      this.url=URL;
      this.pagina=5;
   }


  cargar_todos(){
      let Url=this.url+"/product/getproducts";
      let headers=new HttpHeaders({'Content-Type':'application/json'});

      return this.http.get(Url,{headers});
  }



}
