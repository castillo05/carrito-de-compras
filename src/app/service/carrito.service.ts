import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CarritoService {
  public items:any[]=[];

  constructor() { 
    this.cargarCarrito();
  }

    agregar_carrito(item_parametro:any){
      
       this.items.push(item_parametro);  
       localStorage.setItem("producto",JSON.stringify(this.items));
       return console.log(item_parametro.producto+ 'Agregado al carrito');  
    }

    cargarCarrito(){
      let promesa = new Promise((resolve,reject)=>{
         if (localStorage.getItem("producto")) {
        this.items=JSON.parse(localStorage.getItem("producto"));
         }
         resolve();
      })
  
      return promesa;
     
  
    }

}
