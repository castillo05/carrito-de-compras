import { TabsComponent } from './tabs/tabs.component';
import { ProductoComponent } from './producto/producto.component';
import { PorCategoriaComponent } from './por-categoria/por-categoria.component';
import { CarritoComponent } from "./carrito/carrito.component";
import { CategoriasComponent } from './categorias/categorias.component';
import { LoginComponent } from './login/login.component';
import { LordenesComponent } from './lordenes/lordenes.component';
import { OrdenesDetalleComponent } from './ordenes-detalle/ordenes-detalle.component';
